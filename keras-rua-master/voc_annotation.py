import xml.etree.ElementTree as ET
import csv

def read_csv(csv_path):
    input_id = ['0']
    with open(csv_path, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            if input_id[-1] != row[1]:
                input_id.append(row[1])
    input_id.pop(0)
    input_id.pop(0)
    return input_id
category = read_csv('try.csv')
sets=[('2007', 'train'), ('2007', 'val'), ('2007', 'test')]

classes = category


def convert_annotation(year, image_id, list_file):
    in_file = open('E:/landmark-recognition-2021/keras-yolo3-master/voc/VOCdevkit/VOC%s/Annotations/%s.xml'%(year, image_id))
    tree=ET.parse(in_file)
    root = tree.getroot()

    for obj in root.iter('object'):
        difficult = obj.find('difficult').text
        cls = obj.find('name').text
        if cls not in classes or int(difficult)==1:
            continue
        cls_id = classes.index(cls)
        xmlbox = obj.find('bndbox')
        b = (int(xmlbox.find('xmin').text), int(xmlbox.find('ymin').text), int(xmlbox.find('xmax').text), int(xmlbox.find('ymax').text))
        list_file.write(" " + ",".join([str(a) for a in b]) + ',' + str(cls_id))

wd = 'G:/landmark-recognition-2021/voc'
for year, image_set in sets:
    i = 0
    image_ids = open('D:/landmark-recognition-2021/voc/VOCdevkit/VOC%s/ImageSets/Main/%s.txt'%(year, image_set)).read().strip().split()
    list_file = open('%s_%s.txt'%(year, image_set), 'w')
    for image_id in image_ids:
        list_file.write('%s/VOCdevkit/VOC%s/JPEGImages/%s/%s/%s/%s.jpg'%(wd, year, image_id[0], image_id[1], image_id[2], image_id))
        #  convert_annotation(year, image_id, list_file)
        list_file.write('\n')
        i+=1
        print(i / len(image_ids))
    list_file.close()

