import csv
from PIL import Image
csv_path = 'train.csv'


def read_csv(csv_path):
    input_name = []
    input_id = []
    with open(csv_path, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            input_name.append(row[0])
            input_id.append(row[1])
    input_name.pop(0)
    input_id.pop(0)
    return input_name,input_id


if __name__ == '__main__':
    filename,id = read_csv(csv_path)
    index_path = r"D:\landmark-recognition-2021\voc\VOCdevkit\VOC2007\JPEGImages"
    i = 0
    num = len(filename)
    while i < num:
        firstdir = filename[i][0]
        seconddir = filename[i][1]
        thirddir = filename[i][2]
        image_dir = "D:/landmark-recognition-2021/voc/VOCdevkit/VOC2007/JPEGImages/{0}/{1}/{2}/{3}.jpg".format(firstdir,seconddir,thirddir,filename[i])
        image = Image.open(image_dir)
        with open("E:\\landmark-recognition-2021\\keras-yolo3-master\\voc\\VOCdevkit\\VOC2007\\Annotations\\"+filename[i]+".xml",'w') as f:
            f.writelines('<annotation>\n')
            f.writelines('\t<folder>JPEGImages</folder>\n')
            f.writelines('\t<filename>' + filename[i] + '.jpg/<filename>\n')
            f.writelines('\t<path>D:/landmark-recognition-2021/voc/VOCdevkit/VOC2007/JPEGImages/' +
                         firstdir + '/'+seconddir + '/'+thirddir + '/'+filename[i] + '.jpg</path>\n')
            f.writelines('\t<source>\n\t\t<database>Unknown</database>\n\t</source>\n')
            f.writelines(
                '\t<size>\n\t\t<width>'+str(image.width)+'</width>\n\t\t<height>'+str(image.height)+'</height>\n\t\t<depth>1</depth>\n\t</size>\n')
            f.writelines('\t<segmented>0</segmented>\n')
            f.writelines(
                '\t<object>\n\t\t<name>' + id[i] + '</name>\n\t\t<pose>Unspecified</pose>\n\t\t<truncated>1'
                                                     '</truncated>\n\t\t<difficult>0</difficult>\n\t\t<bndbox>\n\t\t\t<xmin>0</xmin>\n\t\t\t<ymin'
                                                     '>0</ymin>\n\t\t\t<xmax>'+str(image.width)+'</xmax>\n\t\t\t<ymax>'+str(image.height)+'</ymax>\n\t\t</bndbox>\n\t</object>\n')
            f.writelines('</annotation>')
            i = i+1
            print(i/num)
