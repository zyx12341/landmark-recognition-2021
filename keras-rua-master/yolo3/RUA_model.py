"""RUA! Model Defined in Keras."""

from functools import wraps

from tensorflow.keras.layers import Conv2D, Add, ZeroPadding2D, UpSampling2D, Concatenate, MaxPooling2D
from tensorflow.keras.layers import LeakyReLU
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.models import Model
from tensorflow.keras.regularizers import l2

from yolo3.utils import compose


@wraps(Conv2D)
def conv2d(*args, **kwargs):
    conv_kwargs = {'kernel_regularizer': l2(5e-4), 'padding': 'valid' if kwargs.get('strides') == (2, 2) else 'same'}
    conv_kwargs.update(kwargs)
    return conv2d(*args, **conv_kwargs)


def convolutional(*args, **kwargs):
    no_bias_kwargs = {'use_bias': False}
    no_bias_kwargs.update(kwargs)
    return compose(
        conv2d(*args, **no_bias_kwargs),
        BatchNormalization(),
        LeakyReLU(alpha=0.1))


def con_res(x, num_filters, num_blocks):
    x = ZeroPadding2D(((1, 0), (1, 0)))(x)
    x = convolutional(num_filters, (3, 3), strides=(2, 2))(x)
    for i in range(num_blocks):
        y = compose(
            convolutional(num_filters // 2, (1, 1)),
            convolutional(num_filters, (3, 3)))(x)
        x = Add()([x, y])
    return x


def buildingnet(x):
    x = convolutional(32, (3, 3))(x)
    x = con_res(x, 256, 8)  # res8
    x = con_res(x, 512, 8)  # res8
    x = con_res(x, 1024, 4)  # res4
    x = con_res(x, 128, 4)  # res4
    x = con_res(x, 64, 4)  # res4
    return x


def dbl_conv(x, num_filters, out_filters):
    x = compose(
        #   DBL*5
        convolutional(num_filters, (1, 1)),
        convolutional(num_filters * 2, (3, 3)),
        convolutional(num_filters, (1, 1)),
        convolutional(num_filters * 2, (3, 3)),
        convolutional(num_filters, (1, 1)))(x)
    y = compose(
        convolutional(num_filters * 2, (3, 3)),  # DBL
        conv2d(out_filters, (1, 1)))(x)  # conv
    return x, y


def rua(inputs, num_anchors, num_classes):
    buildingnet_1 = Model(inputs, buildingnet(inputs))
    x, y1 = dbl_conv(buildingnet_1.output, 512, num_anchors * (num_classes + 5))  # y1

    x = compose(
        convolutional(256, (1, 1)),  # DBL
        UpSampling2D(2))(x)  # 上采样
    x = Concatenate()([x, buildingnet_1.layers[187].output])  # concat y2
    x, y2 = dbl_conv(x, 256, num_anchors * (num_classes + 5))  # y2

    x = compose(
        convolutional(128, (1, 1)),  # DBL
        UpSampling2D(2))(x)  # 上采样
    x = Concatenate()([x, buildingnet_1.layers[155].output])  # concat y3
    x, y3 = dbl_conv(x, 128, num_anchors * (num_classes + 5))  # y3

    return Model(inputs, [y1, y2, y3])
