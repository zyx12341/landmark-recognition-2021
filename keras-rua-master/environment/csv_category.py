import csv

def read_csv(csv_path):
    input_id = ['0']
    with open(csv_path, 'r') as f:
        reader = csv.reader(f)
        for row in reader:

            if input_id[-1] != row[1]:
                input_id.append(row[1])
    input_id.pop(0)
    input_id.pop(0)
    return input_id
category = read_csv('../train.csv')
with open('category.txt','w') as f:
    for row in category:
        f.writelines(row+'\n')