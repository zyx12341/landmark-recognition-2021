import os
import random

trainval_percent = 0.2 #测试集占0.2
train_percent = 0.8    #训练集占0.8
xmlfilepath = 'E:/landmark-recognition-2021/keras-yolo3-master/voc/VOCdevkit/VOC2007/Annotations'
txtsavepath = 'D:/landmark-recognition-2021/voc/VOCdevkit/VOC2007/ImageSets/Main'
total_xml = os.listdir(xmlfilepath)

num = len(total_xml)
list = range(num)
tv = int(num * trainval_percent)
tr = int(tv * train_percent)
trainval = random.sample(list, tv)
train = random.sample(trainval, tr)

ftrainval = open('D:/landmark-recognition-2021/voc/VOCdevkit/VOC2007/ImageSets/Main/trainval.txt', 'w')
ftest = open('D:/landmark-recognition-2021/voc/VOCdevkit/VOC2007/ImageSets/Main/test.txt', 'w')
ftrain = open('D:/landmark-recognition-2021/voc/VOCdevkit/VOC2007/ImageSets/Main/train.txt', 'w')
fval = open('D:/landmark-recognition-2021/voc/VOCdevkit/VOC2007/ImageSets/Main/val.txt', 'w')

num = len(list)

for i in list:
    name = total_xml[i][:-4] + '\n'
    if i in trainval:
        ftrainval.write(name)
        if i in train:
            ftest.write(name)
        else:
            fval.write(name)
    else:
        ftrain.write(name)
    print(i/num)

ftrainval.close()
ftrain.close()
fval.close()
ftest.close()