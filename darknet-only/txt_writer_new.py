import csv


def read_csv(csv_path):
    input_id = ['0']
    imgs = []
    names = []
    with open(csv_path, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            imgs.append(row[0])
            names.append(row[1])
            if input_id[-1] != row[1]:
                input_id.append(row[1])
    imgs.pop(0)
    names.pop(0)
    input_id.pop(0)
    input_id.pop(0)
    return input_id, imgs, names


category, imgs, names = read_csv('train.csv')
classes = category
i = 0
list_file = open('train.txt', 'w')
while i < len(imgs):
    cls_id = classes.index(names[i])
    list_file.writelines('G:/landmark-recognition-2021/voc/VOCdevkit/VOC2007/JPEGImages/%s/%s/%s/%s.jpg,' % (
    imgs[i][0], imgs[i][1], imgs[i][2], imgs[i]))
    list_file.writelines(str(cls_id) + '\n')
    i += 1
    print(i / len(imgs))
