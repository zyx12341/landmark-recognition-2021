from utils import rand

def picture_resize(annotation_path,input_shape):
    from PIL import Image
    with open(annotation_path,'r',encoding="utf-8") as f:
        lines = f.readlines()
    for line in lines:
        line = line.split(',')
        image = Image.open(line[0])
        iw, ih = image.size
        h, w = input_shape
        jitter = .3
        # resize image
        new_ar = w / h * rand(1 - jitter, 1 + jitter) / rand(1 - jitter, 1 + jitter)
        scale = rand(.25, 2)
        if new_ar < 1:
            nh = int(scale * h)
            nw = int(nh * new_ar)
        else:
            nw = int(scale * w)
            nh = int(nw / new_ar)
        image = image.resize((nw, nh), Image.BICUBIC)

        # place image
        dx = int(rand(0, w - nw))
        dy = int(rand(0, h - nh))
        new_image = Image.new('RGB', (w, h), (128, 128, 128))
        new_image.paste(image, (dx, dy))
        image = new_image

        image.save(line[0])

if __name__ == "__main__":
    picture_resize('./train.txt', (416, 416))