from model import darknet_body
import tensorflow.keras.backend as K
from tensorflow.keras.layers import Input, Lambda
from tensorflow.keras.models import Model
# from model import darknet_loss
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint, ReduceLROnPlateau, EarlyStopping
from utils import get_random_data
import numpy as np
import tensorflow as tf

log_dir = './output/'


# def preprocess_true_boxes(input_shape, num_classes):  # 002
#     y_true = 1
#     return y_true


def data_generator(annotation_lines, batch_size, input_shape, num_classes):
    '''data generator for fit_generator'''
    n = len(annotation_lines)
    i = 0
    while True:
        image_data = []
        y_true = []
        for b in range(batch_size):
            if i == 0:
                np.random.shuffle(annotation_lines)
            image, y_train = get_random_data(annotation_lines[i], input_shape, random=True)
            image_data.append(image)
            y_true.append(y_train)
            i = (i + 1) % n
        image_data = np.array(image_data)
        y_true = tf.keras.utils.to_categorical(y_true, num_classes)
        yield image_data, y_true


if __name__ == "__main__":
    input_shape = (416, 416)
    annotation_path = './train31/train1.txt'
    num_anchors = 3
    num_classes = 5246
    epoch_num = 10
    batch_size = 6
    image_input = Input(shape=(416, 416, 3))
    h, w = input_shape
    print('Create darknet53 network with {} anchors and {} classes.'.format(num_anchors, num_classes))

    darknet = Model(image_input, darknet_body(image_input, num_classes))

    val_split = 0.2
    with open(annotation_path,'r',encoding="utf-8") as f:
        lines = f.readlines()
    np.random.seed(10101)
    np.random.shuffle(lines)
    np.random.seed(None)
    num_val = int(len(lines) * val_split)
    num_train = len(lines) - num_val
    logging = TensorBoard(log_dir=log_dir)
    checkpoint = ModelCheckpoint(log_dir + 'ep{epoch:03d}-loss{loss:.3f}-val_loss{val_loss:.3f}.h5',
                                 monitor='val_loss', save_weights_only=True, save_best_only=False, period=epoch_num/5)

    if True:
        darknet.compile(optimizer=Adam(lr=1e-3), loss=tf.losses.categorical_crossentropy)
        print('Train on {} samples, val on {} samples, with batch size {}.'.format(num_train, num_val, batch_size))
        darknet.fit_generator(data_generator(lines[:num_train], batch_size, input_shape, num_classes),
                              steps_per_epoch=max(1, num_train // batch_size),
                              validation_data=data_generator(lines[num_train:], batch_size, input_shape, num_classes),
                              validation_steps=max(1, num_val // batch_size),
                              epochs=epoch_num, initial_epoch=0,
                              callbacks=[logging, checkpoint])
        darknet.save_weights(log_dir + 'trained_weights_stage_1.h5')

        freeze_layers = len(darknet.layers)
        for i in range(freeze_layers): darknet.layers[i].trainable = True

        if True:
            darknet.compile(optimizer=Adam(lr=1e-4), loss='categorical_crossentropy')

            batch_size = 1
            print('Train on {} samples, val on {} samples, with batch size {}.'.format(num_train, num_val, batch_size))
            darknet.fit_generator(data_generator(lines[:num_train], batch_size, input_shape, num_classes),
                              steps_per_epoch=max(1, num_train // batch_size),
                              validation_data=data_generator(lines[num_train:], batch_size, input_shape, num_classes),
                              validation_steps=max(1, num_val // batch_size),
                              epochs=epoch_num, initial_epoch=50,
                              callbacks=[logging, checkpoint])
            darknet.save_weights(log_dir + annotation_path[10:15] + '.h5')
