# landmark-recognition-2021

Buildingnet for landmark-recognition-2021

## Getting started

Buildingnet is a transform to darknet53. It is mainly designed for recoginize building and architecture.

To understand the principle, it is better to do some research on the structure of darknet53 and yolov3. 

## Installation

```
cd existing_repo
git clone https://gitlab.com/zyx12341/landmark-recognition-2021.git
```

Then use tarin.py to train datasets and rua.py to detect test_image.

***
## Name

We call our network Rua! The similar sounds of cat roar.

## Authors and acknowledgment

China Hfut software enginering zyx and nll

## License

Just use it!

## Project status

Testing

## Learn more

```
---------------------------------------------
| times |Type    Filters    Size    Output  |
---------------------------------------------
|   1   |conv    32         3*3     256*256 |
---------------------------------------------
|   1   |conv    256        3*3/2   128*128 |
---------------------------------------------
|       |conv    128        1*1             |
|   8   |conv    256        3*3             |
|       |res                        128*128 |
---------------------------------------------
|   1   |conv    512        3*3/2   64*64   |
---------------------------------------------
|       |conv    256        1*1             |
|   8   |conv    512        3*3             |
|       |res                        64*64   |
---------------------------------------------
|   1   |conv    1024       3*3/2   32*32   |
---------------------------------------------
|       |conv    512        1*1             |
|   4   |conv    1024       3*3             |
|       |res                        32*32   |
---------------------------------------------
|   1   |conv    128        3*3/2   16*16   |--->result1
---------------------------------------------
|       |conv    64         1*1             |
|   4   |conv    128        3*3             |
|       |res                        16*16   |
---------------------------------------------
|   1   |conv    64        3*3/2    8*8     |--->result2
---------------------------------------------
|       |conv    32         1*1             |
|   4   |conv    64         3*3             |--->result3
|       |res                        8*8     |
---------------------------------------------
```
